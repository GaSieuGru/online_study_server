package com.study.exceptions;

public class InternalErrorException extends RuntimeException {
    /**
     * Internal Error Exception
     */
    private static final long serialVersionUID = -3845455461178772826L;

    public InternalErrorException(Throwable t) {
    	super(t);
    }
    
    public InternalErrorException(String mes) {
        // TODO Auto-generated constructor stub
        super(mes);
    }

}
