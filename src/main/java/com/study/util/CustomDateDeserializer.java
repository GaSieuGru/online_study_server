package com.study.util;

import java.io.IOException;
import java.util.Date;

import com.study.exceptions.InternalErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class CustomDateDeserializer extends StdDeserializer<Date> {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ssZ";

    public CustomDateDeserializer() {
        this(null);
    }
 
    public CustomDateDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        
        try
        {
            String dateString = p.getText();
            return DateTimeUtil.toUtilDateUTC(dateString, DATE_PATTERN);
        }
        catch (Exception e) {
            logger.debug(e.getLocalizedMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
    }

}
