/*
package com.study.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.dynagility.ironportalcore.ldap.LdapConfig;
import com.dynagility.ironportalcore.model.User;
import com.dynagility.ironportalcore.service.UserService;

@Configuration
@EnableWebSecurity
@Import(value = {LdapConfig.class})
public class SecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    public static final int SESSION_INTEVAL_TIME = 60 * 60;
    public static final String AUTHENTICATION_TOKEN = "x-auth-token";


    @Value("${ldap.search.path:}")
    private String ldapSearchPath;

    @Autowired
    private UserService userService;

    @Autowired
    private LdapConfig ldapConfig;

    @Autowired 
    private LdapAuthenticationProvider ldapAuthenticationProvider;

    @Autowired
    private TokenAuthFilter tokenAuthFilter;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Bean
    public LdapAuthenticationProvider ldapAuthenticationProvider() {
    	logger.debug("Bind UserDN: " + ldapConfig.contextSource().getUserDn());
    	logger.debug("Bind UserDN: " + ldapConfig.contextSource().getPassword());
        FilterBasedLdapUserSearch userSearch = new FilterBasedLdapUserSearch("", ldapSearchPath, ldapConfig.contextSource());
        BindAuthenticator authenticator = new BindAuthenticator(ldapConfig.contextSource());
        authenticator.setUserSearch(userSearch);

        return new LdapAuthenticationProvider(authenticator) {
            @Override
            protected Collection<? extends GrantedAuthority> loadUserAuthorities(DirContextOperations userData,
                    String username, String password) {
                User user = userService.getUserByEmail(username);

                if (user == null || user.getRole() == null) {
                    return AuthorityUtils.NO_AUTHORITIES;
                }

                return Arrays.asList(new SimpleGrantedAuthority("ROLE_" + user.getRole()));
            }
        };
    }

    public Authentication authenticate(String email, String password) {
        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
                email, password);
        return authenticationManager.authenticate(authRequest);
    }

    public String updateSession(Authentication authen, HttpServletRequest request) {
        final String temporarySessionKey = "AUTH";
        String token = UUID.randomUUID() + authen.getName();
        HttpSession session = request.getSession();
        logger.debug("Session id: " + session.getId());
        session.setAttribute(temporarySessionKey, authen);

        return token;
    }
    
    public void removeSession(HttpServletRequest request) {
        final String temporarySessionKey = "AUTH";
        HttpSession session = request.getSession();
        logger.debug("Session id: " + session.getId());
        session.setMaxInactiveInterval(0);
        session.removeAttribute(temporarySessionKey);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
        .and()
        .authorizeRequests()
        .antMatchers("/assets/**").permitAll()
        .antMatchers("/**").permitAll()
        .and().authorizeRequests()
        .anyRequest().authenticated()
        .and()
        .csrf().disable();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(ldapAuthenticationProvider);
    }
}*/
