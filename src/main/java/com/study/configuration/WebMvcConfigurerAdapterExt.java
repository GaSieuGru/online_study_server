package com.study.configuration;

import java.io.IOException;

import com.study.interceptors.CORSInterceptor;
import com.study.interceptors.CacheBustingInterception;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;

@Configuration
public class WebMvcConfigurerAdapterExt extends WebMvcConfigurerAdapter {
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:/public/" };

    @Autowired
    public CORSInterceptor cors;

    @Autowired
    public CacheBustingInterception cacheBusting;
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(cors);
        registry.addInterceptor(cacheBusting);
    }

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if (!registry.hasMappingForPattern("/**")) {
            registry.addResourceHandler("/**")
                    .addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS)
                    .resourceChain(true)
                    .addResolver(new PathResourceResolver() {
                        @Override
                        protected Resource getResource(String resourcePath, Resource location) throws IOException {
                            resourcePath = "index.html";
                            return super.getResource(resourcePath, location);
                        }
                    });
        }

        registry.addResourceHandler("/assets/**")
                .addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS)
                .resourceChain(true)
                .addResolver(new PathResourceResolver() {
                    @Override
                    protected Resource getResource(String resourcePath, Resource location) throws IOException {
                        resourcePath = "/assets/" + resourcePath;
                        return super.getResource(resourcePath, location);
                    }
                });
    }
}
