package com.study.controller;

import com.study.enums.UserStatusEnum;
import com.study.exceptions.NotAuthenticatedException;
import com.study.model.User;
import com.study.service.UserService;
import com.study.validator.RequestBodyValidator;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = GeneralBaseController.BASE_URL_API)
public class GeneralBaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    public static final String BASE_URL_API = "/api";
    public static final String AUTHENTICATION_URL_API = "/authentication";

    @Autowired
    UserService userService;

//    @CrossOrigin(origins = "http://localhost:8001")
    @RequestMapping(value = GeneralBaseController.AUTHENTICATION_URL_API, method = RequestMethod.POST)
    public ResponseEntity<?> authenticateUser(@RequestBody Map<String, Object> body, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String emailKey = "email";
        String passwordKey = "password";
        Map<String, Object> badLogin = new HashMap<>();
        badLogin.put("isBadLogin", true);

        if (RequestBodyValidator.validate(body, emailKey, passwordKey)) {
            String email = body.get(emailKey).toString();
            String password = body.get(passwordKey).toString();
            User user = userService.findByEmailAndPassword(email, password);

            if(user == null) {
                throw new NotFoundException("The user is not found.");
            } else if (user.getStatus().equals(UserStatusEnum.INACTIVE.getValue())) {
                throw new NotAuthenticatedException(String.format("User with '%s' email has been disabled", email));
            }
            userService.save(user);
            return new ResponseEntity<>(user, HttpStatus.OK);
        }

        return new ResponseEntity<>(badLogin, HttpStatus.BAD_REQUEST);
    }
}
