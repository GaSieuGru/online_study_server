package com.study.enums;

public enum UserStatusEnum {
    ACTIVE("ACTIVE"),
    PENDING_VERIFICATION("PENDING_VERIFICATION"),
    INACTIVE("INACTIVE"),
    LOCKED("LOCKED");

    private String value;

    UserStatusEnum(String value) {
        this.value = value;
    }

    public static UserStatusEnum getByValue(String value) {
        for (UserStatusEnum e : values()) {
            if(e.value.equals(value)) {
                return e;
            }
        }

        return null;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

