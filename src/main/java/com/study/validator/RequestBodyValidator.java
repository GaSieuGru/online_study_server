package com.study.validator;

import java.util.Map;

public class RequestBodyValidator {

    public static boolean validate(Map<String, Object> body, String... names) {
        if(names != null) {
            for (String name : names) {
                if (!body.containsKey(name) || body.get(name) == null) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
