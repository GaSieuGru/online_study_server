package com.study.model;

import java.lang.reflect.Field;
import java.text.ParseException;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;

import com.study.exceptions.InternalErrorException;
import org.springframework.util.ReflectionUtils;

import com.study.util.DateTimeUtil;

@MappedSuperclass
public abstract class BaseModel {
    @Transient
    protected Class<?> clazz;

    public void setClazz() {}

    protected BaseModel(Class<?> clazz) {
        this.clazz = clazz;
    }

    @PrePersist
    public void onPreInsert() {
        try {
            Field createdAt = null;
            try {
                createdAt = ReflectionUtils.findField(clazz, "createdAt");
            } catch (Exception ex) {}
            
            Field isArchived = null;
            try {
            	isArchived = ReflectionUtils.findField(clazz, "isArchived");
            } catch (Exception ex) {}

            if(createdAt != null) {
                boolean accessible = createdAt.isAccessible();
                createdAt.setAccessible(true);
                if (createdAt.get(this) == null) {
                    createdAt.set(this, DateTimeUtil.currentDateTimeUTC());
                }
                createdAt.setAccessible(accessible);
            }

            if (isArchived != null) {
                boolean accessible = isArchived.isAccessible();
                isArchived.setAccessible(true);
                isArchived.set(this, false);
                isArchived.setAccessible(accessible);
            }

            onPreInsertExtend();
        } catch (Exception ex) {
            throw new InternalErrorException(ex.getMessage());
        }
    }

    @PreUpdate
    public void onPreUpdate() {
        try {
            Field updatedAt = null;
            try {
                updatedAt = ReflectionUtils.findField(clazz, "updatedAt");
            }
            catch (Exception ex) {
            }
            if( updatedAt != null ) {
                boolean accessible = updatedAt.isAccessible();
                updatedAt.setAccessible(true);
                updatedAt.set(this, DateTimeUtil.currentDateTimeUTC());
                updatedAt.setAccessible(accessible);
            }
            onPreUpdateExtend();
        } catch (Exception ex) {
            throw new InternalErrorException(ex.getMessage());
        }
    }

    public void onPreInsertExtend() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, ParseException {
        try {
            Field madeOn = null;
            try {
                madeOn = this.clazz.getDeclaredField("madeOn");
            } catch (NoSuchFieldException ex){
            }
            
            if (madeOn != null) {
                boolean accessibleMadeOn = madeOn.isAccessible();
                madeOn.setAccessible(true);
                madeOn.set(this, DateTimeUtil.currentDateTimeUTC());
                madeOn.setAccessible(accessibleMadeOn);
            }
        } catch (Exception ex) {
            throw new InternalErrorException(ex.getMessage());
        }
    }

    public void onPreUpdateExtend() {}
}
