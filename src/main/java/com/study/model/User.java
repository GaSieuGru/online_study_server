package com.study.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.study.util.CustomDateDeserializer;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Transient;
import java.util.Date;


@Entity
@Table(name = "user")
@DynamicUpdate
public class User extends BaseModel {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "email", nullable = true)
    private String email;

    @Column(name = "first_name", nullable = true)
    private String firstName;

    @Column(name = "last_name", nullable = true)
    private String lastName;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "password", nullable = false)
    private String password;

    @JsonFormat(pattern= CustomDateDeserializer.DATE_PATTERN)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    @Column(name = "created_at", nullable = true)
    private Date createdAt;

    @JsonFormat(pattern= CustomDateDeserializer.DATE_PATTERN)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    @Column(name = "updated_at", nullable = true)
    private Date updatedAt;

    @JsonFormat(pattern= CustomDateDeserializer.DATE_PATTERN)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    @Column(name = "last_login", nullable = true)
    private Date lastLogin;

    public User() {
        super(User.class);
    }

    public User(String id) {
        super(User.class);
        this.id = id;
    }

    public User(String id, String email, String firstName, String lastName) {
        super(User.class);

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return id != null ? id.equals(user.id) : user.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
